# Reads the filename from the initial args given to the script
filename = ARGV.first

# Accesses the file
txt = open(filename)

# prints out the file name
puts "Here's your file #{filename}:"
# executes the .read function on the file, prints the result
print txt.read
txt.close

# Gets the filename using gets.chomp
print "Type the filename again: "
file_again = $stdin.gets.chomp

# Accesses the file
txt_again = open(file_again)

# executes the .read function on the file, prints the result
print txt_again.read
txt.close
