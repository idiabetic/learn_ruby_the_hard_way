filename = ARGV.first

puts "We're going to erase #{filename}"
puts "If you don't want that, hit CTRL-c (^c)."
puts "If you do want that, hit return. "

$stdin.gets

puts "Opening the file..."
target = open(filename, 'w')


puts "Now I'm going to ask you for three lines. "

puts "line 1: "
line1 = $stdin.gets.chomp
puts "Line 2: "
line2 = $stdin.gets.chomp
puts "line 3: "
line3 = $stdin.gets.chomp

puts "I'm going  to write these to the file. "

target.write("#{line1} \n #{line2} \n #{line3} \n")


puts "And finally, we close it. "
target.close
