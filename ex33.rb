numbers = []

def loop(list, increment)
  increment = increment.to_i
  i = 0
  (0..6).each do |i|
    puts "At the top i is #{i}"
    list.push(i)
    puts "Numbers now: ", list
    puts "At the bottom i is #{i}"
  end
  return list
end

loop(numbers, 1)

puts "The numbers: "

numbers.each {|num| puts num }
