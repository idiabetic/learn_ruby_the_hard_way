# Assigns a value to types_of_people
types_of_people = 10
# defines a string with the value of types_of_people
x = "There are #{types_of_people} types of people."
# assigns string values to binary and do not
binary = "binary"
do_not = "don't"
# defines a formatted string with the values from binary and do_not
y = "Those who know #{binary} and those who #{do_not}."

# prints the variables defined above
puts x
puts y

# prints the variables defined above in formatted strings
puts "I Said: '#{x}'."
puts "I also said: '#{y}'."

# Assings a value to hilarious
hilarious = false
# assigns a formatted string to joke_evaluation using the value assigned to
# hilarious
joke_evaluation = "Isn't that joke so funny?! #{hilarious}"

# prints joke_evaluation
puts joke_evaluation

# defines two different strings to two different variables
w = "This is the left side of..."
e = "a string with a right side."

# combines two string variables to print one sentence.
puts w + e
