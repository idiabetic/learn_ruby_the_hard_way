name = 'Patrick O. Shobe'
age = 26
height = 69 # Inches
height_cent = height * 2.54
weight = 140 # lbs
weight_kilo = weight * 0.453592
eyes = 'Blue'
teeth = 'White'
hair = 'Brown'

puts "Let's talk about #{name}."
puts "He's #{height} inches tall or #{height_cent} centimeters tall."
puts "He's #{weight} pounds or #{weight_kilo} Kilos."
puts "Actually that's not too heavy"
puts "He's got #{eyes} eyes and #{hair} hair."
puts "His teeth are usually #{teeth} depending on the coffee."

puts "If I add #{age}, #{height}, and #{weight} I get #{age + height + weight}."
