# Prints a string
puts "Mary had a little lamb."
# Prints a string
puts "Its fleece was white as #{'snow'}."
# Prints a string
puts "And everywhere that Mary went."
# Prints a string * 10
puts "." * 10 # What'd that do?

# Assings single characters to a variable
end1 = "c"
end2 = "h"
end3 = "e"
end4 = "e"
end5 = "s"
end6 = "e"
end7 = "b"
end8 = "u"
end9 = "r"
end10 = "g"
end11 = "e"
end12 = "r"

# prints out the first 6 characters with no new line
print end1 + end2 + end3 + end4 + end5 + end6
# printsthe last 6 characters
puts end7 + end8 + end9 + end10 + end11 + end12
