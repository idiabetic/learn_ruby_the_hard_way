# Sets the value of cars to 100
cars = 100
# Sets the value of space_in_a_car to 4.0
space_in_a_car = 4.0
# Sets the value of drivers to 30
drivers = 30
# Sets the value of passengers to 90
passengers = 90
# Calculates the number of cars_not_driven by subtracting the number of cars
# from the number of drivers.
cars_not_driven = cars - drivers
# Sets the value of cars_driven to equal drivers
cars_driven = drivers
# Calculates the carpool_capacity by multiplying the cars driven by the spaces
# available in a car
carpool_capacity = cars_driven * space_in_a_car
# Calculates the number of average_passengers_per_car by dividing passengers
# and cars_driven
average_passengers_per_car = passengers / cars_driven

# prints the number of cars available.
puts "There are #{cars} cars available."
# Prints the number of drivers available
puts "There are only #{drivers} drivers available."
# prints the number of cars_not_driven
puts "There will be #{cars_not_driven} empty cars today."
# prints the total carpool_capacity
puts "We can transport #{carpool_capacity} people today."
# Prints the number of passengers
puts "We have #{passengers} to carpool today."
# prints the number of passengers per car
puts "We need to put about #{average_passengers_per_car} in each car."
