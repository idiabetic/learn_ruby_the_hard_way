# Prints out a string
puts "I will now count my chickens:"

# Prints out a string + a computation, the computation is between the {}.
# You can place any ruby code between the {}.
puts "Hens #{25.0 + 30.0 / 6.0}"
puts "Roosters #{100.0 - 25.0 * 3.0 % 4.0}"

# Prints out a string
puts "Now I will count the eggs:"

# Prints out a computation
puts 3.0 + 2.0 + 1.0- 5.0 + 4.0 % 2.0 - 1.0 / 4.0 + 6.0

# Prints a string
puts "Is it true that 3 + 2 < 5 - 7?"

# Performs a coparison between two values, returns a boolean value
puts 3.0 + 2.0 < 5.0 - 7.0

# Prints out a string + a computation, the computation is between the {}.
# You can place any ruby code between the {}.
puts "What is 3 + 2? #{3.0 + 2.0}"
puts "What is 5 - 7? #{5.0 - 7.0}"

# Prints a string
puts "Oh, that's why it's false."

# Prints a string
puts "How about some more."

# Prints out a string + a computation, the computation is between the {}.
puts "Is it greater? #{5.0 > -2.0}"
puts "Is it greater or equal? #{5.0 >= -2.0}"
puts "Is it less or equal? #{5.0 <= -2.0}"
